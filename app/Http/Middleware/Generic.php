<?php

namespace App\Http\Middleware;
//use Symfony\Component\HttpFoundation\Response;

use Closure;

class Generic
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->wantsJson()){
            return $next($request);
        }
        return response()->view('generic');
    }
}
