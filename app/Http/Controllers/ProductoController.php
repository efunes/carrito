<?php

namespace App\Http\Controllers;

use Auth;
use Carbon\Carbon;
use App\Reserva;
use App\Producto;
use App\Categoria;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class ProductoController extends Controller
{
    public function list(Request $request){
        $query = Producto::with('categoria','accesorios');

        if($request->input('opciones')){
            return [
                'categorias' => Categoria::all()
            ];
        }
        if($request->input('to_sale')){
            $query->where('creador_id','=',Auth::id())
            ->withCount(["reservas" => function($q){
                $q->where('reservas.estado', '=', 'reserva');
            }]);;
        }

        $limit = $request->input('limit')? : 10;
        $offset = $request->input('offset')? : 0;       


        return [
            'size' => $query->count(),
            'values' => $query->limit($limit)->offset($offset)->get()
        ];
    }
    public function store(Request $request){
        if($request->input('just_image')){
            $dir = 'public/tmp/'.Auth::id();
            $code = Storage::putFile($dir, $request->file);
            return [
                'code'=> $code,
                'thumbnail' => Storage::url($code),
            ];
        }
        $data = $request->validate([
            'nombre' => 'required|min:1',
            'descripcion' => 'required|min:1',
            'categoria_id' => 'required',
            'precio' => 'required',
            'imagen' => 'required',
            'stock' => 'required',
        ]);

        $data['creador_id'] = Auth::id();

        $producto = Producto::create($data);

        $producto->accesorios()->createMany($request->input('accesorios'));

        $extension = substr($producto->imagen,strrpos($producto->imagen,'.'));
        Storage::move($producto->imagen,'public/producto/' . $producto->id . $extension);
        $producto->imagen = Storage::url('public/producto/' . $producto->id . $extension);
        $producto->save();


        return $producto;
    }
    public function update(Request $request, $id){
        $data = $request->validate([
            'nombre' => 'required|min:1',
            'descripcion' => 'required|min:1',
            'categoria_id' => 'required',
            'precio' => 'required',
            'stock' => 'required'
        ]);

        Producto::where('id',"=",$id)->update($data);

        $new_imagen = $request->input('imagen');

        $producto = Producto::find($id);
        $producto->accesorios()->delete();
        $producto->accesorios()->createMany($request->input('accesorios'));

        if($new_imagen){
            $extension = substr($new_imagen,strrpos($producto->imagen,'.'));
            $new_file = 'public/producto/' . $producto->id . $extension;

            if(Storage::exists($new_file)){
                Storage::delete($new_file);
            }

            Storage::move($new_imagen,$new_file);
            $producto->imagen = Storage::url('public/producto/' . $producto->id . $extension);
            $producto->save();
        }
        

        return $producto;
    }
    public function item(Request $request, $id){
        return Producto::with('accesorios','categoria')->find($id);
    }

    public function reservas(Request $request, $id){
        $query = Reserva::with('producto','solicitante')
            ->where('producto_id','=',$id)
            ->where('estado','reserva');
        return [
            'size' => $query->count(),
            'values' => $query->get(),
            'producto'=>Producto::find($id),
        ];
    }

    public function delete(Request $request, $id){
        $producto =  Producto::find($id);
        $producto->delete();
    }
}
