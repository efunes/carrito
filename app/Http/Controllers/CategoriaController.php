<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;

class CategoriaController extends Controller
{
    public function list(Request $request){
        $query = Categoria::withCount('productos');

        $limit = $request->input('limit')? : 10;
        $offset = $request->input('offset')? : 0;       


        return [
            'size' => $query->count(),
            'values' => $query->limit($limit)->offset($offset)->get()
        ];
    }
    public function store(Request $request){
        $data = $request->validate([
            'nombre' => 'required|min:1',
        ]);

        $categoria = Categoria::create($data);

        return $categoria;
    }
    public function update(Request $request, $id){
        $data = $request->validate([
            'nombre' => 'required|min:1',
        ]);

        $categoria = Categoria::where('id',"=",$id)->update($data);

        return $categoria;
    }
    public function delete(Request $request, $id){
        $categoria = Categoria::find($id);

        if($categoria){
            $categoria->delete();
        }
    }    
}
