<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Reserva;
use App\Producto;

class CarritoController extends Controller
{
    public function list(Request $request){
        $query = Reserva::with('producto','solicitante')
            ->where('estado','=','carrito')
            ->where('solicitante_id','=',Auth::id());

        $limit = $request->input('limit')? : 100;
        $offset = $request->input('offset')? : 0;


        return [
            'size' => $query->count(),
            'values' => $query->limit($limit)->offset($offset)->get()
        ];
    }
    public function store(Request $request){
        $data = $request->validate([
            'cantidad' => 'required',
            'producto_id' => 'required',
        ]);
        
        $data['estado'] = 'carrito';
        $data['precio'] = Producto::find($data['producto_id'])->precio * $data['cantidad'];
        $data['solicitante_id'] = Auth::id();        

        $reserva = Reserva::create($data);

        return $reserva;
    }
    public function update(Request $request, $id){
        $data = $request->validate([
            'cantidad' => 'required|min:1',
        ]);

        $reserva = Reserva::with('producto')
            ->where('id',"=",$id)
            ->where('estado','=','carrito')
            ->where('solicitante_id','=',Auth::id())
            ->first();

        if($reserva){
            $reserva->cantidad = $data['cantidad'];
            $reserva->precio = $reserva->producto->precio * $reserva->cantidad;
            $reserva->save();
        }
    }
    public function delete(Request $request, $id){
        $reserva = Reserva::find($id);

        if($reserva){
            $reserva->delete();
        }
    }    
}
