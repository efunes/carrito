<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Reserva;
use App\Producto;

class ReservaController extends Controller
{
    public function list(Request $request){
        $query = Reserva::with('producto','solicitante')
            ->where('estado','=','reserva')
            ->where('solicitante_id','=',Auth::id());

        $limit = $request->input('limit')? : 100;
        $offset = $request->input('offset')? : 0;


        return [
            'size' => $query->count(),
            'values' => $query->limit($limit)->offset($offset)->get()
        ];
    }

    public function reservar(Request $request){

        $reservas = Reserva::with('producto')
            ->where('estado','=','carrito')
            ->where('solicitante_id','=',Auth::id())
            ->get();
            
        foreach($reservas as $key=>$value){
            $value->estado = 'reserva';
            $value->save();
            $value->producto->stock-= $value->cantidad;
            $value->producto->save();
        }
    }    
}
