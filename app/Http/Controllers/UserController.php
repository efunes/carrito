<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    

    public function login (Request $request){

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {            
            return $this->user_data($request);
        }
        abort(403,'Credenciales inválidas');
    }

    public function registro (Request $request){
        $data = $request->validate([
            'name' => 'required|min:3',
            'email' => 'required|min:3|unique:users',
            'password' => 'required|min:3',
        ]);
        $data['password'] = Hash::make($data['password']);
        $data['is_admin'] = User::query()->count() == 0;
        $user = User::create($data);
        return $user;
    }

    public function logout(Request $request){
        Auth::logout();
    }

    public function user_data(Request $request){
        $user = Auth::user();
        if($user){
            return $user;
        }
        return [
            'id' => null
        ];
    }
}
