<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Producto extends Model
{
    use SoftDeletes;
    protected $casts = [
        'stock' => 'float',
        'precio' => 'float',
    ];

    protected $fillable = [
        'nombre','descripcion',
        'imagen','stock','precio',
        'categoria_id','creador_id'
    ];

    public function accesorios(){
        return $this->hasMany('App\Accesorios','producto_id');
    }
    public function categoria (){
        return $this->belongsTo('App\Categoria','categoria_id');
    }
    public function creador (){
        return $this->belongsTo('App\User','creador_id');
    }
    public function reservas(){
        return $this->hasMany('App\Reserva','producto_id');
    }
}
