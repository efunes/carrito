<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reserva extends Model
{
    protected $casts = [
        'precio' => 'float',
        'cantidad' => 'float',
    ];
    protected $fillable = [
        'cantidad','producto_id',
        'solicitante_id','precio','estado'
    ];

    public function solicitante(){
        return $this->belongsTo('App\User','solicitante_id');
    }
    public function producto(){
        return $this->belongsTo('App\Producto','producto_id');
    }
}