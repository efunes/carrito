import list from './CategoriaList.vue'

export default [
    {
        path:'/productos/categorias',
        component:list,
        name:'productos.categorias.list'
    },
    {
        path:'/productos/categorias/:id',
        name:'productos.categorias.item',
    },
]