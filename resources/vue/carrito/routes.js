import list from './Carrito.vue'
import reservas from './Reserva.vue'

export default [
    {
        path:'/carrito',
        component:list,
        name:'carrito'
    },
    {
        path:'/reservas',
        component:reservas,
        name:'reservas'
    },
    {
        path:'/carrito/:id',
        name:'carrito.item'
    },
    
]