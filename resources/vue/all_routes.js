import home_page from './home.vue'
import user_routes from "./user/routes";
import carrito_route from './carrito/routes'
import producto_route from './producto/routes'
import categoria_routes from './categoria/routes'

export default [
    {
        path:'/',
        component:home_page,
        name:'home',
    },
    ...user_routes,
    ...carrito_route,
    ...producto_route,
    ...categoria_routes,
]

