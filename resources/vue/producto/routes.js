import form from './ProductoForm.vue'
import list from './ProductoList.vue'
import item from './Producto.vue'
import reserva from './ProductoReserva.vue'


export default [
    {
        path:'/productos/',
        component:list,
        name:'productos.list'
    },
    {
        path:'/productos/crear',
        component:form,
        name:'productos.crear'
    },
    {
        path:'/productos/:id/editar',
        component:form,
        name:'productos.editar',
    },
    {
        path:'/productos/:id/reservas',
        component:reserva,
        name:'productos.reserva',
    },
    {
        path:'/productos/:id(\\d+)',
        name:'productos.item',
        component:item,
    },
]