import login from './login.vue'
import registro from './registro.vue'

export default [
    {
        path:'/login/',
        component:login,
        name:'user.login'
    },
    {
        path:'/logout/',
        name:'user.logout',
    },
    {
        path:'/user/',
        name:'user.data'
    },
    {
        path:'/registro/',
        component:registro,
        name:'user.registro'
    }

]