<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="/css/app.css">
    </head>
    <body>
        <div id="app">
            <nav-bar></nav-bar>
            <router-view></router-view>
        </div>
        <script src="/js/app.js"></script>
    </body>
</html>