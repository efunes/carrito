<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('generic')->group(function () {
    $no_action = function (){};
        
    Route::get('/','ProductoController@list');
    Route::get('/user','UserController@user_data');    
});


Route::middleware(['only_guest','generic'])->group(function () {
    $no_action = function (){};
        
    Route::get('/login',$no_action);
    Route::post('/login','UserController@login');
    Route::get('/registro',$no_action);
    Route::post('/registro','UserController@registro');
});


Route::middleware(['only_user','generic'])->group(function () {
    $no_action = function (){};
    
    Route::post('/logout','UserController@logout');

    Route::get('/productos','ProductoController@list');
    Route::post('/productos','ProductoController@store');
    Route::get('/productos/crear',$no_action);
    Route::get('/productos/{id}/editar',$no_action);
    Route::get('/productos/{id}/reservas','ProductoController@reservas');
    Route::get('/productos/{id}','ProductoController@item')->where(['id'=>'[0-9][1-9]*']);
    Route::put('/productos/{id}','ProductoController@update');
    Route::delete('/productos/{id}','ProductoController@delete');

    Route::get('/carrito','CarritoController@list');
    Route::post('/carrito','CarritoController@store');
    Route::put('/carrito/{id}','CarritoController@update');
    Route::delete('/carrito/{id}','CarritoController@delete');

    Route::get('/reservas','ReservaController@list');
    Route::post('/reservas','ReservaController@reservar');
    
});


Route::middleware(['only_user','only_admin','generic'])->group(function () {
    $no_action = function (){};
    
    Route::get('/productos/categorias','CategoriaController@list');
    Route::post('/productos/categorias','CategoriaController@store');
    Route::put('/productos/categorias/{id}','CategoriaController@update');
    Route::delete('/productos/categorias/{id}','CategoriaController@delete');
    
});
